# Test java básico (java-test-001)

Proyecto para prueba de reclutamiento en lenguaje Java básico. Consiste en una prueba sencilla que permite evaluar algunos aspectos básicos del conocimiento del postulante en el lenguaje Java.

## Instrucciones para su uso

Esta sección incluye las instrucciones para instalar y ejecutar la aplicación de test java básico.

### Requisitos de software instalado

Con el fin de instalar y ejecutar la aplicación, se requiere contar con el siguiente software base.

- ***GIT:*** Con el fin de descargar el código fuente.
- ***JDK 1.8:*** Con el fin de construir y probar la aplicación.
- ***IDE de desarrollo:*** [Visual Studio Code](https://code.visualstudio.com/download) o [IntelliJ](https://www.jetbrains.com/idea/download) sirven para los propósitos de la presente aplicación.

### Instrucciones de instalación

Lo primero que se debe hacer es descargar el código fuente del proyecto desde Bitbucket. Tenga en consideración que debe reemplazar el `<usuario_bbs>` por el nombre del usuario bitbucket que realiza la descarga.

```bash
$ git clone https://<usuario_bbs>@bitbucket.org/Tinet/java-test-001.git
Cloning into 'java-test-001'...
remote: Counting objects: 16, done.
remote: Compressing objects: 100% (8/8), done.
remote: Total 16 (delta 1), reused 0 (delta 0)
Unpacking objects: 100% (16/16), done.
```

Luego de esto, usted ***debe abrir el proyecto con alguno de los IDE de desarrollo*** mencionados. Con esto se puede comenzar con la prueba.

### Instrucciones del problema a resolver

El objetivo de la prueba es verificar la capacidad de resolución de problemas de postulante, en el contexto de desarrollo en lenguaje Java. Adicionalmente se validará la comprensión del usuario de conceptos de gestión de repositorios, en particular usando Git como herramienta SCM.

1. Crear una **nueva rama de feature** llamada `feature/implementacion-<yyyyMMdd>-<nombre>-<apellido>` a partir de develop, donde se debe reemplazar la fecha actual en formato año, mes y día (yyyyMMdd), junto con el nombre y el apellido del postulante sin acentos, espacios ni caracteres especiales. Por ejemplo `feature/implementacion-20180715-ramon-valdes`.
2. **Cambie la rama** actual del repositorio local a la nueva rama creada.
3. Resuelva el problema. En este caso se trata de **implementar el método** `cl.tinet.test.JavaTest001#buscarNombres(int)`, siguiendo la descripción de la javadocumentación del método (el método debe hacer lo que dice su documentación).
4. Resuelva a la vez el **problema de compilación** que existe en el proyecto. Existe más de una opción para esto.
5. **Sincronice el código en su repositorio local**, indicando una glosa descriptiva del cambio realiado.
6. **Publique** su rama actual en el repositorio centralizado de origen.
7. **Notifique a su entrevistador** que ya publicó su rama de desarrollo en el repositorio central.
