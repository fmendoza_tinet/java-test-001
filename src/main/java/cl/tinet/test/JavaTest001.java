package cl.tinet.test;

import java.util.Map;

/**
 * Clase generada como prueba para los postulantes a desarrollador Backend Java.
 */
public static class JavaTest001 {

    /**
     * Listado de nombres sobre el que se realizará la búsqueda.
     */
    private static final String[] NOMBRES = {
            "abel", "rodrigo", "juan", "diego", "patricio",
            "fernando", "pedro", "zacarías", "isaac", "jacob",
            "leon", "hernán", "carlos", "david", "daniel",
            "nelson", "alvaro", "ernesto", "maría", "jesus"};


    /**
     * Método principal de la clase.
     * <p>
     * Realiza la invocación al método que busca los nombres, y despliega en la consola de salida estandar
     * el resultado de la búsqueda.
     *
     * @param args son los argumentos del programa enviados por línea de comandos.
     */
    public static void main(String[] args) {
        System.out.println("Buscando nombres de longitud 5...");
        Map<String, Integer> map = buscarNombres(5);
        System.out.println("Nombres y Posiciones encontrados: " + map);
    }

    /**
     * Permite obtener el conjunto de los nombres, y sus respectivas posiciones dentro del listado <code>NOMBRES</code>
     * cuya cantidad de caracteres corresponda con la <code>longitud</code> especificada como argumento.
     *
     * @param longitud en caracteres de los nombres a identificar y retornar en el conjunto.
     * @return conjunto de nombres, y sus respectivas posiciones, encontrados.
     */
    public Map<String, Integer> buscarNombres(int longitud) {
        // TODO implementar lógica.
        return null;
    }
}
