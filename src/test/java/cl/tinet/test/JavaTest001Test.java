package cl.tinet.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map;
import org.junit.Test;

/**
 * Pruebas unitarias de la clase.
 */
public class JavaTest001Test {

    // Largo solicitado.
    private static final int LARGO = 5;

    // Nombre que existe en el arreglo que cumple con el largo 5
    private static final String NOMBREVALIDO = "pedro";

    @Test
    public void testClass() {
        JavaTest001 clase = new JavaTest001();
    }

    @Test
    public void testMainMethod() {
        JavaTest001.main(null);
    }

    /**
     * Validar que encuentre nombres de largo 5.
     */
    @Test
    public void validarSalidaLargoCinco() {
        assertTrue("Datos no encontrados", JavaTest001.buscarNombres(LARGO).size() > 0);
    }

    /**
     * Validar que encuentre nombres de largo 4.
     */
    @Test
    public void validarSalidaLargoCuatro() {
        assertTrue("Datos no encontrados", JavaTest001.buscarNombres(LARGO - 1).size() > 0);
    }

    /**
     * Validar que no retorne datos si el largo es negativo.
     */
    @Test
    public void validaSalidaLargoNegativo() {
        assertTrue("No deberia encontrar nombres", JavaTest001.buscarNombres(-1).isEmpty());
    }

    /**
     * Validar que todos los nombres encontrados tengan largo 5.
     */
    @Test
    public void validaLargoDeTodosLosNombres() {
        JavaTest001.buscarNombres(LARGO)
        .forEach((k,v) -> { assertTrue("Largo no valido", k.length() == LARGO); });
    }

    /**
     * Valida que existe el nombre "pedro" dentro de la respuesta.
     */
    @Test
    public void validarNombreClave() {
        String encontrado = JavaTest001.buscarNombres(LARGO)
            .entrySet()
            .stream()
            .filter(e -> e.getKey().equals(NOMBREVALIDO))
            .map(Map.Entry::getKey)
            .findFirst()
            .orElse(null);

        assertNotNull(encontrado);
    }
}
